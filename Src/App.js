import React from "react";
import Dadjokes from "./Dadjokes";

function App() {
  return (
    <div className="App">
      <Dadjokes />
    </div>
  );
}

export default App;
