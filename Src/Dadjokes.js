import React, { useEffect, useState } from "react";
import axios from "axios";
import "./Dadjokes.css";

const Dadjokes = () => {
  const [dadJokes, setDadJokes] = useState([]);

  const fetchData = async () => {
    for (var i = 0; i < 20; i++) {
      const res = await axios.get("https://icanhazdadjoke.com/", {
        headers: { Accept: "application/json" }
      });
      setDadJokes(dadJokes => [...dadJokes, res.data.joke]);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="joke">
      <h1>jokes</h1>
      {dadJokes &&
        dadJokes.map((j, index) => (
          <ul className="data" key={index}>
            <li>{j}</li>
          </ul>
        ))}
    </div>
  );
};

export default Dadjokes;
